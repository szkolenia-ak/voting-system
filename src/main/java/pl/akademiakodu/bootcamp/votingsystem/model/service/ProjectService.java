package pl.akademiakodu.bootcamp.votingsystem.model.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithStatusDto;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithVotesDto;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.VoteDto;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Vote;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.ProjectRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VoteRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VotersRepository;

import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProjectService {
    ProjectRepository projectRepository;
    VotersRepository votersRepository;
    VoteRepository voteRepository;

    @Autowired
    ProjectService projectService;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, VotersRepository votersRepository, VoteRepository voteRepository) {
        this.projectRepository = projectRepository;
        this.votersRepository = votersRepository;
        this.voteRepository = voteRepository;
    }

    public VoteDto makeVote(Long projectId, Long voterId, Integer voteValue){
        Optional<Project> projectOptional = projectRepository.findById(projectId);
        if(!projectOptional.isPresent()){
            throw new RuntimeException("Project doesn't exist.");
            //return null;
        }

        Optional<Voter> voterOptional = votersRepository.findById(voterId);
        if(!voterOptional.isPresent()){
            throw new RuntimeException("Voter doesn't exist.");
        }

        Project project = projectOptional.get();
        if(project.getActive() == false){
            throw new RuntimeException("Project not active.");
        }

        //voteRepository.existsByProjectAndVoter(project, voterOptional.get());
        boolean voteExists = voteRepository.existsByProjectIdAndVoterId(projectId, voterId);
        if (voteExists){
            throw new RuntimeException("Vote already exists.");
        }

        Vote vote = new Vote(voterOptional.get(), project, voteValue);
        Vote savedVote = voteRepository.save(vote);

        return new ModelMapper().map(savedVote, VoteDto.class);


//        if(projectOptional.isPresent()){
//            Optional<Voter> voterOptional = votersRepository.findById(voterId);
//            if(voterOptional.isPresent()){
//                Project project = projectOptional.get();
//                if(project.getActive() == true){
//                    //....
//                } else {
//
//                }
//            } else {
//
//            }
//        }
    }

    public ProjectWithStatusDto changeStatus(Long projectId, Boolean active){
        Optional<Project> projectOptional = projectRepository.findById(projectId);
        if(!projectOptional.isPresent()){
            throw new RuntimeException("Project doesn't exist.");
        }

        Project project = projectOptional.get();
        if(project.getActive().equals(active)){
            return new ModelMapper().map(project, ProjectWithStatusDto.class);
        }

        project.setActive(active);
        Project savedProject = projectRepository.save(project);

        return new ModelMapper().map(savedProject, ProjectWithStatusDto.class);

//        ProjectWithStatusDto projectWithStatusDto = new ProjectWithStatusDto();
//        projectWithStatusDto.setId(savedProject.getId());
//        projectWithStatusDto.setProjectName(savedProject.getProjectName());
//        projectWithStatusDto.setActive(savedProject.getActive());
//        return projectWithStatusDto;

    }

    @Transactional
    public Optional<ProjectWithVotesDto> getProjectWithVotes(Long projectId){
//        return getProjectFromVotes(projectId);
        //return getProjectFromDatabase(projectId);
        return getProjectFromProject(projectId);
    }

    private Optional<ProjectWithVotesDto> getProjectFromDatabase(Long projectId) {
        return projectRepository.findByIdWithVotes(projectId);
    }

    private Optional<ProjectWithVotesDto> getProjectFromVotes(Long projectId) {
        Optional<Project> projectOptional = projectRepository.findById(projectId);
        if(!projectOptional.isPresent()){
            return Optional.empty();
        }
        List<Vote> votes = voteRepository.findAllByProjectId(projectId);
        int positiveVotes = 0, negativeVotes = 0;
        int cnt = 0;
        for (Vote vote : votes) {
            cnt++;
            Integer voteValue = vote.getVoteValue();

//            if(cnt % 2 == 0) {
//                System.out.println(vote.getVoter().getVoterName());
//            }

            if (voteValue > 0) {
                positiveVotes++;
            } else {
                negativeVotes++;
            }
        }

        ProjectWithVotesDto projectWithVotesDto =
                new ModelMapper().map(projectOptional.get(), ProjectWithVotesDto.class);
        projectWithVotesDto.setPositiveVotes(positiveVotes);
        projectWithVotesDto.setNegativeVotes(negativeVotes);
        return Optional.of(projectWithVotesDto);
    }

    private Optional<ProjectWithVotesDto> getProjectFromProject(Long projectId) {
        Optional<Project> projectOptional = projectRepository.findById(projectId);
        if(!projectOptional.isPresent()){
            return Optional.empty();
        }

        Project project = projectOptional.get();
        int positiveVotes = 0, negativeVotes = 0;

        for (Vote vote : project.getVotes()) {
            Integer voteValue = vote.getVoteValue();
            if (voteValue > 0) {
                positiveVotes++;
            } else {
                negativeVotes++;
            }
        }
        ProjectWithVotesDto projectWithVotesDto =
                new ModelMapper().map(project, ProjectWithVotesDto.class);
        projectWithVotesDto.setPositiveVotes(positiveVotes);
        projectWithVotesDto.setNegativeVotes(negativeVotes);
        return Optional.of(projectWithVotesDto);
    }

    @Transactional
    public Vote saveMany(){
        SecureRandom secureRandom = new SecureRandom();

        Voter voter = votersRepository.save(new Voter("Voter [no-tran] " + secureRandom.nextInt()));

        int i = secureRandom.nextInt();
        Project project = projectRepository.save(
                new Project("Project [no-tran] " + i,
                        "Description of project " + i,
                        true)
        );

        if (true) {
            throw new RuntimeException("something went wrong...");
        }

        Vote vote = new Vote();
        vote.setProject(project);
        vote.setVoter(voter);
        vote.setVoteValue(secureRandom.nextBoolean() ? 1 : -1);

        Vote savedVote = voteRepository.save(vote);

        return savedVote;
    }

    @Transactional
    public Object[] saveProject(){
        SecureRandom secureRandom = new SecureRandom();
        Voter voter = votersRepository.save(new Voter("Voter [no-tran] " + secureRandom.nextInt()));
        int i = secureRandom.nextInt();
        Project project = projectRepository.save(
                new Project("Project [no-tran] " + i,
                        "Description of project " + i,
                        true)
        );
        if(true){
            throw new RuntimeException("asd");
        }
        return new Object[]{voter, project};
    }

    public Vote saveVote(Voter voter, Project project){
        SecureRandom secureRandom = new SecureRandom();
        Vote vote = new Vote();
        vote.setProject(project);
        vote.setVoter(voter);
        vote.setVoteValue(secureRandom.nextBoolean() ? 1 : -1);

        Vote savedVote = voteRepository.save(vote);

        return savedVote;
    }

    public Vote saveMany2(){
        Object[] objects = projectService.saveProject();
        Vote vote = projectService.saveVote((Voter)objects[0], (Project)objects[1]);
        return vote;
    }
}
