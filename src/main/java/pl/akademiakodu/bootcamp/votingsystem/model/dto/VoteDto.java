package pl.akademiakodu.bootcamp.votingsystem.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class VoteDto {
    private Long id;
    private Long voterId;
    private Long projectId;
    private Integer voteValue;
    private Date added;
}
