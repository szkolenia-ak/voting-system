package pl.akademiakodu.bootcamp.votingsystem.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectDto {
    private Long id;
    private String projectName;
    private String projectDescription;
}
