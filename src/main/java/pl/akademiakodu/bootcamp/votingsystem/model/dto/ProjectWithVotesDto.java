package pl.akademiakodu.bootcamp.votingsystem.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProjectWithVotesDto {
    private String projectName;
    private String projectDescription;
    private boolean active;

    private long positiveVotes;
    private long negativeVotes;

    public ProjectWithVotesDto(String projectName, boolean active) {
        this.projectName = projectName;
        this.active = active;
    }
}
