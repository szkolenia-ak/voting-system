package pl.akademiakodu.bootcamp.votingsystem.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;

@Repository
public interface VotersRepository extends JpaRepository<Voter, Long>{
}
