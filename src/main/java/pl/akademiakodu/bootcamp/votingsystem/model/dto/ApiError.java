package pl.akademiakodu.bootcamp.votingsystem.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ApiError {
    private String errorId;
    private String errorMessage;

    private Date errorDate = new Date();

    public ApiError(String errorId, String errorMessage) {
        this.errorId = errorId;
        this.errorMessage = errorMessage;
    }
}
