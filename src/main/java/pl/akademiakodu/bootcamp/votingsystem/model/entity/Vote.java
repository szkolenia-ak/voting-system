package pl.akademiakodu.bootcamp.votingsystem.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table (uniqueConstraints = {
        @UniqueConstraint(columnNames = {"voter_id", "project_id"})
})
@NoArgsConstructor
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voter_id")
    private Voter voter;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    private Integer voteValue;
    private Date added = new Date();

    public Vote(Voter voter, Project project, Integer voteValue) {
        this.voter = voter;
        this.project = project;
        this.voteValue = voteValue;
    }
}
