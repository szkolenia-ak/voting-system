package pl.akademiakodu.bootcamp.votingsystem.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithVotesDto;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findAllByOrderByProjectName();

    @Query("SELECT new pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithVotesDto(p.projectName, p.projectDescription, p.active, sum(case when v.voteValue > 0 then 1 else 0 end), sum(case when v.voteValue < 0 then 1 else 0 end)) FROM Project p LEFT JOIN Vote v ON p.id = v.project WHERE p.id = :projectId")
    Optional<ProjectWithVotesDto> findByIdWithVotes(@Param("projectId") Long projectId);

    @Query("SELECT new pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithVotesDto(p.projectName, p.active) FROM Project p WHERE p.id = :projectIdParam")
    Optional<ProjectWithVotesDto> findByIdWithProjection(@Param("projectIdParam") Long projectId);

}
