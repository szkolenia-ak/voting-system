package pl.akademiakodu.bootcamp.votingsystem.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
//@Table(name = "ak_project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String projectName;
    private String projectDescription;
    private Boolean active;

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    private Set<Vote> votes;

    public Project(String projectName, String projectDescription, Boolean active) {
        this.projectName = projectName;
        this.projectDescription = projectDescription;
        this.active = active;
    }
}
