package pl.akademiakodu.bootcamp.votingsystem.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Vote;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;

import java.util.List;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long>{
    boolean existsByProjectIdAndVoterId(Long projectId, Long voterId);
    boolean existsByProjectAndVoter(Project project, Voter voter);

    List<Vote> findAllByProjectId(Long projectId);
}
