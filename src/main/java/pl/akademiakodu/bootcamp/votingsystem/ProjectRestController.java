package pl.akademiakodu.bootcamp.votingsystem;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.*;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Vote;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.ProjectRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VoteRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VotersRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.service.ProjectService;

import java.security.SecureRandom;
import java.util.*;

@RestController
public class ProjectRestController {
    ProjectRepository projectRepository;
    ProjectService projectService;

    @Autowired
    VotersRepository votersRepository;
    @Autowired
    VoteRepository voteRepository;

    @Autowired
    public ProjectRestController(ProjectRepository projectRepository, ProjectService projectService) {
        this.projectRepository = projectRepository;
        this.projectService = projectService;
    }

    @GetMapping("/api/projects")
    public ResponseEntity<List<ProjectDto>> getProjects(){
        List<Project> projects = projectRepository.findAll();
        Collections.sort(projects, new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getProjectName().compareToIgnoreCase(o2.getProjectName());
            }
        });

        //List<Project> projectsSorted = projectRepository.findAllByOrderByProjectName();

        List<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projects) {
            projectDtoList.add((new ModelMapper()).map(project, ProjectDto.class));
        }

        return ResponseEntity.ok(projectDtoList);
    }

    @PostMapping("/api/project/{projectId}/vote")
    public ResponseEntity<VoteDto> makeVote(@PathVariable Long projectId,
                                            @RequestParam Long voterId,
                                            @RequestParam Integer voteValue){
        VoteDto voteDto = projectService.makeVote(projectId, voterId, voteValue);
        return ResponseEntity.status(200).body(voteDto);

    }

    @PutMapping("/api/project/{projectId}/active")
    public ResponseEntity<ProjectWithStatusDto> changeStatus(
            @PathVariable Long projectId, @RequestParam Boolean active){

        return ResponseEntity.ok(projectService.changeStatus(projectId, active));
    }

    @GetMapping("/api/project/{projectId}")
    public ResponseEntity<Object> projectDetails(
            @PathVariable Long projectId
    ){
        Optional<ProjectWithVotesDto> optionalProjectWithVotesDto =
                projectService.getProjectWithVotes(projectId);

        if(!optionalProjectWithVotesDto.isPresent()){
            //throw new RuntimeException("Project doesn't exist.");
            ApiError apiError = new ApiError("123", "Project not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiError);
        }

        return ResponseEntity.ok(optionalProjectWithVotesDto.get());

    }

    @GetMapping("/api/test/no-tran")
    public ResponseEntity<Object> testWithoutTransaction(){
        SecureRandom secureRandom = new SecureRandom();

        Voter voter = votersRepository.save(new Voter("Voter [no-tran] " + secureRandom.nextInt()));

        int i = secureRandom.nextInt();
        Project project = projectRepository.save(
                new Project("Project [no-tran] " + i,
                        "Description of project " + i,
                        true)
        );

        if (true) {
            throw new RuntimeException("something went wrong...");
        }

        Vote vote = new Vote();
        vote.setProject(project);
        vote.setVoter(voter);
        vote.setVoteValue(secureRandom.nextBoolean() ? 1 : -1);

        Vote savedVote = voteRepository.save(vote);

        return ResponseEntity.ok(savedVote);
    }

    @GetMapping("/api/test/with-tran")
    public ResponseEntity<Object> testWithTransaction(){
        //Vote vote = projectService.saveMany();

//        Object[] objects = projectService.saveProject();
//        Vote vote = projectService.saveVote((Voter)objects[0], (Project)objects[1]);

        Vote vote = projectService.saveMany2();
        return ResponseEntity.ok(vote);
    }

}
