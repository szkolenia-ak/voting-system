package pl.akademiakodu.bootcamp.votingsystem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.ProjectRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VoteRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VotersRepository;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class DbTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    VotersRepository votersRepository;

    @Test
    public void testMakingVotesWithDb() throws Exception {
        Project project = new Project("name", "description", true);
        Voter voter = new Voter("voter");

        projectRepository.save(project);
        votersRepository.save(voter);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/project/{projectId}/vote", "1")
                .param("voterId", "3")
                .param("voteValue", "1")
        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                //.andExpect(MockMvcResultMatchers.content().json("{\"id\":null,\"voterId\":2,\"projectId\":1,\"voteValue\":1,\"added\":\"2018-04-08T15:57:47.154+0000\"}"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.voteValue").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.voterId").value(2))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"));
    }

}
