package pl.akademiakodu.bootcamp.votingsystem;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Voter;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.ProjectRepository;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.VotersRepository;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
//@ActiveProfiles("test")
public class VotingSystemApplicationTests {

	@Autowired
	private VotersRepository votersRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Test
	@Ignore
	public void createVoters() {
		for (int i = 1; i <= 5; i++) {
			votersRepository.save(new Voter("Voter " + i));
		}
	}

	@Test
	@Ignore
	public void createProjects() {
		for (int i = 1; i <= 5; i++) {
			projectRepository.save(
					new Project("Project " + i,
							"Description of project " + i,
							true)
			);
		}
	}


}
