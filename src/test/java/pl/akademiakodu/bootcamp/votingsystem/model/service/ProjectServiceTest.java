package pl.akademiakodu.bootcamp.votingsystem.model.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.akademiakodu.bootcamp.votingsystem.model.dto.ProjectWithStatusDto;
import pl.akademiakodu.bootcamp.votingsystem.model.entity.Project;
import pl.akademiakodu.bootcamp.votingsystem.model.repository.ProjectRepository;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProjectServiceTest {

    @MockBean
    ProjectRepository projectRepository;

    @Autowired
    ProjectService projectService;

    @Test
    public void shouldInvokeSaveProjectExactlyOnceAndWithRightStatus(){
        //Mockito.mock(ProjectRepository.class); //zalatwia nam to @MockBean
        Mockito.when(projectRepository.findById(anyLong()))
                .thenReturn(Optional.of(new Project("project name", "project description", true)));
        Mockito.when(projectRepository.save(any()))
                //.thenReturn(new Project("project name", "project description", true));
                .thenAnswer(invocation -> invocation.getArgument(0));


        ProjectWithStatusDto projectWithStatusDto = projectService.changeStatus(2L, false);


        Mockito.verify(projectRepository, VerificationModeFactory.times(1)).save(ArgumentMatchers.any());

        ArgumentCaptor<Project> argProject = ArgumentCaptor.forClass(Project.class);
        Mockito.verify(projectRepository).save(argProject.capture());
        assertEquals(false, argProject.getValue().getActive());

        assertEquals(false, projectWithStatusDto.isActive());

    }

}